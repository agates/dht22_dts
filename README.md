# dht22_dts
This project reads the humidity data from all a dht22 probe
and sends it over the [Domain Type System (DTS)](https://gitlab.com/agates/domain-type-system).
