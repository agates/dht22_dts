# -*- coding: utf-8 -*-

#    dht22_dts
#    Copyright (C) 2018  Alecks Gates
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import itertools
import logging
from datetime import datetime, timezone
from timeit import default_timer as timer

import capnpy
from domaintypesystem import DomainTypeSystem
try:
    import Adafruit_DHT
except ImportError:  # pragma: no cover
    class AdafruitStub:
        DHT22 = None

        def __init__(self):
            pass

        @staticmethod
        def read_retry(sesnor, pin, retries=None, delay_seconds=None):
            return None, None

    Adafruit_DHT = AdafruitStub()

HumidityEvent = capnpy.load_schema('src.dht22_dts.schema.humidity_event', pyx=False).HumidityEvent


def current_timestamp():
    # returns floating point timestamp in seconds
    return datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()


def current_timestamp_nanoseconds():
    return current_timestamp() * 1e9


def read_sensor(pin, sensor, retries=10, delay_seconds=2):
    try:
        # Reads data from the probe, retying 10 times with 2 second delays
        humidity, temperature = Adafruit_DHT.read_retry(sensor, pin, retries=retries, delay_seconds=delay_seconds)
        return humidity, temperature
    except RuntimeError as e:
        logging.error(e)
        return None, None


async def send_humidity(dts, pin, sensor):
    humidity, _ = read_sensor(pin, sensor)
    if humidity:
        logging.info("Humidity: {0}%".format(humidity))
        await dts.send_struct(HumidityEvent(humidity=bytes(str(humidity), 'UTF-8'),
                                            timestamp=int(current_timestamp_nanoseconds()),
                                            model=b'DHT22',
                                            serial_number=None))
    else:
        logging.warning("No humidity read from probe")


async def monitor_humidity(dts, sensor=Adafruit_DHT.DHT22, pin=2, loop=None):
    logging.info("Monitoring humidity for sensor on pin {0}".format(pin))
    if loop is None:  # pragma: no cover
        loop = asyncio.get_event_loop()

    # while True: # changed this to itertools.repeat to be able to mock it out for testing
    for _ in itertools.repeat(None):
        start_time = timer()
        await send_humidity(dts, pin, sensor)
        await asyncio.sleep(30 - (timer() - start_time), loop=loop)


def run(dts=None, loop=None):
    logging.basicConfig(level=logging.INFO)

    if not loop:  # pragma: no cover
        loop = asyncio.new_event_loop()

    if not dts:  # pragma: no cover
        dts = DomainTypeSystem(loop=loop)

    register_coro = dts.register_pathway(HumidityEvent)
    monitor_coro = monitor_humidity(dts)

    if not loop.is_running():  # pragma: no cover
        loop.run_until_complete(register_coro)
        loop.create_task(monitor_coro)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            loop.close()
    else:
        return loop.create_task(register_coro), loop.create_task(monitor_coro)

    if not dts:  # pragma: no cover
        dts.close()
