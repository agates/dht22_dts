@0x832b8bacac1d2ea6;

using TimePointInNs = UInt64;
# Nanoseconds since the epoch.

struct HumidityEvent {
    humidity @0 :Text;
    timestamp @1 :TimePointInNs;
    model @2 :Text;
    serialNumber @3 :Text;
}