from setuptools import find_packages, setup

with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='dht22-dts',
    version='0.1.0',
    description='dht22 probe integration with DTS',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Alecks Gates',
    author_email='agates@mail.agates.io',
    keywords=[],
    url='https://gitlab.com/agates/dht22_dts',
    python_requires='>=3.6',
    classifiers=(
        'Development Status :: 4 - Beta',
        'Framework :: AsyncIO',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Home Automation',
    ),
    install_requires=[
        'Adafruit_Python_DHT;platform_machine=="arm"',
        'capnpy~=0.6',
        'domaintypesystem~=0.1'
    ],
    extras_require={
    },
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    package_data={'dht22_dts': ['schema/*']},
    entry_points={
        'console_scripts': [
            'run_dht22_dts=dht22_dts.dht22_dts:run'
        ],
    }
)
