# -*- coding: utf-8 -*-

#    dht22_dts
#    Copyright (C) 2018  Alecks Gates
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
from datetime import datetime
from unittest.mock import MagicMock, Mock

from domaintypesystem import DomainTypeSystem
import pytest

from dht22_dts import dht22_dts


class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


def test_current_timestamp(mocker):
    mock = Mock(return_value=datetime(1970, 1, 1, 0, 0, 0, 0))

    class FakeDatetime:
        utcnow = mock

    mocker.patch.object(dht22_dts, 'datetime', new_callable=FakeDatetime)
    timestamp = dht22_dts.current_timestamp()

    mock.assert_called_once_with()
    pytest.approx(timestamp, 0)


def test_current_timestamp_nanoseconds(mocker):
    mocker.patch.object(dht22_dts, 'current_timestamp', return_value=1.0)

    timestamp = dht22_dts.current_timestamp_nanoseconds()

    # noinspection PyUnresolvedReferences
    dht22_dts.current_timestamp.assert_called_once_with()
    pytest.approx(timestamp, 1e9)


def test_read_sensor(mocker):
    mock_humidity, mock_temperature = 10.0, 20.0
    mocker.patch.object(dht22_dts.Adafruit_DHT, 'read_retry', return_value=(mock_humidity, mock_temperature))

    pin = 2
    sensor = 1
    retries = 10
    delay_seconds = 2
    humidity, temperature = dht22_dts.read_sensor(pin, sensor, retries=retries, delay_seconds=delay_seconds)

    dht22_dts.Adafruit_DHT.read_retry.assert_called_once_with(sensor, pin, retries=retries, delay_seconds=delay_seconds)
    pytest.approx(humidity, mock_humidity)
    pytest.approx(temperature, mock_temperature)


def test_read_sensor_none(mocker):
    mock_humidity, mock_temperature = None, None
    mocker.patch.object(dht22_dts.Adafruit_DHT, 'read_retry', return_value=(mock_humidity, mock_temperature))

    pin = 2
    sensor = 1
    retries = 10
    delay_seconds = 2
    humidity, temperature = dht22_dts.read_sensor(pin, sensor, retries=retries, delay_seconds=delay_seconds)

    dht22_dts.Adafruit_DHT.read_retry.assert_called_once_with(sensor, pin, retries=retries, delay_seconds=delay_seconds)
    assert humidity is mock_humidity
    assert temperature is mock_temperature


def test_read_sensor_runtime_error(mocker):
    mocker.patch.object(dht22_dts.logging, 'error')
    mock = mocker.patch.object(dht22_dts.Adafruit_DHT, 'read_retry')
    mock.side_effect = RuntimeError('foo')

    pin = 2
    sensor = 1
    retries = 10
    delay_seconds = 2
    humidity, temperature = dht22_dts.read_sensor(pin, sensor, retries=retries, delay_seconds=delay_seconds)

    dht22_dts.Adafruit_DHT.read_retry.assert_called_once_with(sensor, pin, retries=retries, delay_seconds=delay_seconds)
    # noinspection PyUnresolvedReferences
    dht22_dts.logging.error.assert_called_once_with(mock.side_effect)
    assert humidity is None
    assert temperature is None


@pytest.mark.asyncio
async def test_send_humidity(mocker):
    dts = mocker.Mock()
    dts_send_struct_stub = mocker.stub(name='dts_send_struct_stub')

    # Need a stub wrapper since the provided stub isn't async
    async def stub_wrapper(foo):
        return dts_send_struct_stub(foo)

    dts.send_struct = stub_wrapper
    mock_humidity, mock_temperature = 10.0, 20.0
    mocker.patch.object(dht22_dts, 'read_sensor', return_value=(mock_humidity, mock_temperature))
    mock_current_timestamp_nanoseconds = 1.5
    mocker.patch.object(dht22_dts, 'current_timestamp_nanoseconds', return_value=mock_current_timestamp_nanoseconds)
    mock_humidity_event = {'humidity': mock_humidity, 'timestmap': int(mock_current_timestamp_nanoseconds)}
    mocker.patch.object(dht22_dts, 'HumidityEvent', return_value=mock_humidity_event)

    pin = 2
    sensor = 1
    await dht22_dts.send_humidity(dts, pin, sensor)

    # noinspection PyUnresolvedReferences
    dht22_dts.read_sensor.assert_called_once_with(pin, sensor)
    dts_send_struct_stub.assert_called_once_with(mock_humidity_event)
    dht22_dts.HumidityEvent.assert_called_once_with(humidity=bytes(str(mock_humidity), 'UTF-8'),
                                                    timestamp=int(mock_current_timestamp_nanoseconds),
                                                    model=b'DHT22',
                                                    serial_number=None)


@pytest.mark.asyncio
async def test_send_humidity_none(mocker):
    mocker.patch.object(dht22_dts.logging, 'warning')
    mock_humidity, mock_temperature = None, None
    mocker.patch.object(dht22_dts, 'read_sensor', return_value=(mock_humidity, mock_temperature))

    pin = 2
    sensor = 1
    await dht22_dts.send_humidity(None, pin, sensor)

    # noinspection PyUnresolvedReferences
    dht22_dts.read_sensor.assert_called_once_with(pin, sensor)
    # noinspection PyUnresolvedReferences
    dht22_dts.logging.warning.assert_called_once_with("No humidity read from probe")


@pytest.mark.asyncio
async def test_monitor_humidity(mocker, event_loop):
    mocker.patch.object(dht22_dts.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(dht22_dts, 'send_humidity', new_callable=AsyncMock)
    mocker.patch.object(dht22_dts, 'timer', return_value=0)
    mocker.patch.object(dht22_dts.asyncio, 'sleep', new_callable=AsyncMock)

    dts = object()
    pin = 2
    sensor = 1
    await dht22_dts.monitor_humidity(dts, sensor=sensor, pin=pin, loop=event_loop)

    # noinspection PyUnresolvedReferences
    dht22_dts.send_humidity.assert_called_once_with(dts, pin, sensor)


@pytest.mark.asyncio
async def test_monitor_humidity_twice(mocker, event_loop):
    count = 2
    mocker.patch.object(dht22_dts.itertools, 'repeat', return_value=range(count))
    mocker.patch.object(dht22_dts, 'send_humidity', new_callable=AsyncMock)
    mocker.patch.object(dht22_dts, 'timer', return_value=0)
    mocker.patch.object(dht22_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await dht22_dts.monitor_humidity(None, sensor=None, pin=None, loop=event_loop)

    # noinspection PyUnresolvedReferences
    assert dht22_dts.send_humidity.call_count == count
    # noinspection PyUnresolvedReferences
    assert dht22_dts.asyncio.sleep.call_count == count


@pytest.mark.asyncio
async def test_monitor_humidity_ten(mocker, event_loop):
    count = 10
    mocker.patch.object(dht22_dts.itertools, 'repeat', return_value=range(count))
    mocker.patch.object(dht22_dts, 'send_humidity', new_callable=AsyncMock)
    mocker.patch.object(dht22_dts, 'timer', return_value=0)
    mocker.patch.object(dht22_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await dht22_dts.monitor_humidity(None, sensor=None, pin=None, loop=event_loop)

    # noinspection PyUnresolvedReferences
    assert dht22_dts.send_humidity.call_count == count
    # noinspection PyUnresolvedReferences
    assert dht22_dts.asyncio.sleep.call_count == count


@pytest.mark.asyncio
async def test_monitor_humidity_sleep(mocker, event_loop):
    mocker.patch.object(dht22_dts.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(dht22_dts, 'send_humidity', new_callable=AsyncMock)
    mocker.patch.object(dht22_dts, 'timer', return_value=10)
    mocker.patch.object(dht22_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await dht22_dts.monitor_humidity(None, sensor=None, pin=None, loop=event_loop)

    # noinspection PyUnresolvedReferences
    dht22_dts.asyncio.sleep.assert_called_once_with(30, loop=event_loop)


@pytest.mark.asyncio
async def test_run(mocker, event_loop):
    dts_register_pathway_stub = mocker.stub(name='dts_register_pathway_stub')

    class DtsStub:
        def __init__(self):
            pass

        async def register_pathway(self, _):
            dts_register_pathway_stub(_)
            return None

    dts = DtsStub()
    mocker.patch.object(dht22_dts, 'monitor_humidity', new_callable=AsyncMock)

    register_task, monitor_task = dht22_dts.run(dts=dts, loop=event_loop)
    await register_task
    await monitor_task

    dts_register_pathway_stub.assert_called_once_with(dht22_dts.HumidityEvent)
    # noinspection PyUnresolvedReferences
    dht22_dts.monitor_humidity.assert_called_once_with(dts)


@pytest.mark.asyncio
async def test_integration_dts(mocker, event_loop):
    mock_humidity, mock_temperature = 10.0, None
    mocker.patch.object(dht22_dts, 'read_sensor', return_value=(mock_humidity, mock_temperature))

    test_queue = asyncio.Queue()

    async def handle_humidity(event, address, received_timestamp_nanoseconds):
        await test_queue.put(event)

    with DomainTypeSystem(loop=event_loop) as dts_receive:
        await asyncio.sleep(.1)
        await dts_receive.register_pathway(dht22_dts.HumidityEvent)
        await dts_receive.handle_type(dht22_dts.HumidityEvent,
                                      data_handlers=(
                                          handle_humidity,
                                      )
                                      )
        with DomainTypeSystem(loop=event_loop) as dts_send:
            await asyncio.sleep(.1)
            await dts_send.register_pathway(dht22_dts.HumidityEvent)
            await dht22_dts.send_humidity(dts_send, None, None)

            humidity_event = await test_queue.get()
    pytest.approx(mock_humidity, float(humidity_event.humidity))
    test_queue.task_done()
